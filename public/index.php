<?php


include_once __DIR__ . '/../vendor/autoload.php';


$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {

    $r->addRoute('GET', '/', [\App\Controller\TaskController::class, 'list']);
    $r->addRoute('GET', '/list/{page:\d+}', [\App\Controller\TaskController::class, 'list']);
    $r->addRoute('GET', '/list/', [\App\Controller\TaskController::class, 'list']);

    $r->addRoute('GET', '/add/', [\App\Controller\TaskController::class, 'form']);
    $r->addRoute('POST', '/add/', [\App\Controller\TaskController::class, 'add']);

    $r->addGroup('/admin', function (FastRoute\RouteCollector $r) {
        $r->addRoute('GET', '/login/', [\App\Controller\AdminController::class, 'login']);
        $r->addRoute('POST', '/login/', [\App\Controller\AdminController::class, 'auth']);
        $r->addRoute('GET', '/logout/', [\App\Controller\AdminController::class, 'logout']);
        $r->addRoute('GET', '/task/{id:\d+}', [\App\Controller\AdminController::class, 'getTask']);
        $r->addRoute('POST', '/task/{id:\d+}', [\App\Controller\AdminController::class, 'updateTask']);
    });
});

// Fetch method and URI from somewhere
$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = $_SERVER['REQUEST_URI'];

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);


$core = new \App\Core();
$containerBuilder = $core->getContainerBuilder();
$routeInfo = $dispatcher->dispatch($httpMethod, $uri);
switch ($routeInfo[0]) {
    case FastRoute\Dispatcher::NOT_FOUND:
        // ... 404 Not Found
        echo $containerBuilder->get(Twig\Environment::class)->render('404.html.twig');
        break;
    case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        $allowedMethods = $routeInfo[1];
        echo "405\n";
        break;
    case FastRoute\Dispatcher::FOUND:
        $class = $routeInfo[1][0];
        $method = $routeInfo[1][1];

        $vars = $routeInfo[2];

        $setParams = [];
        $r = new ReflectionMethod($class, $method);
        $params = $r->getParameters();

        foreach ($params as $param) {
            if (empty($vars[$param->getName()])) {
                if ($containerBuilder->has($param->getType())) {
                    $setParams[] = $containerBuilder->get($param->getType());
                } else {
                    $setParams[] = null;
                }

                continue;
            }

            $setParams[]  = $vars[$param->getName()];

        }


        $object = new $class(
            $containerBuilder->get(Twig\Environment::class),
            $containerBuilder->get(Symfony\Component\HttpFoundation\Request::class)
        );

        echo $object->$method(...$setParams);

        break;
}
