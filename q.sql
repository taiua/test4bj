CREATE TABLE tasks
(
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) default NULL,
	text VARCHAR(255) default NULL,
	email VARCHAR(255) default NULL,
	status BOOLEAN default NULL,
	changed BOOLEAN default NULL
);

CREATE INDEX index_name ON tasks (name(5));
CREATE INDEX index_email ON tasks(email(5));
CREATE INDEX index_status ON tasks(status);