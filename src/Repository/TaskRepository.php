<?php


namespace App\Repository;

use App\Entity\Task;

/**
 * Class TaskRepository
 * @package App\Repository
 */
class TaskRepository
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * TaskRepository constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return int
     */
    public function count() : int
    {
        $sth = $this->pdo->query(
            'SELECT COUNT(*) as `count`  FROM `tasks`'
        );
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);
        return $result['count'];
    }

    /**
     * @param int $id
     * @return Task|null
     */
    public function get(int $id) : ?Task
    {

        $sth = $this->pdo->prepare(
            'SELECT `id`, `name`, `email`, `text`, `status`, `changed`  FROM `tasks` WHERE `id`=:id  LIMIT 1'
        );

        $sth->bindValue(':id', $id, \PDO::PARAM_INT);
        $sth->execute();
        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        if (!$result) {
            return null;
        }

        return $this->initTask($result);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $modifier
     * @return array
     */
    public function getList(int $limit, int $offset = 0, string $orderBy = 'id', string $modifier = 'DESC') : array
    {
        $params = [1 => 'id', 'name', 'email', 'text', 'status', 'changed'];
        $modifiers = ['ASC' => 'ASC', 'DESC' => 'DESC'];
        $orderNumber = array_search($orderBy, $params, true);
        if (!is_int($orderNumber)) {
            $orderNumber = 1;
        }

        $modifier = $modifiers[$modifier] ?? 'DESC';

        $sql = 'SELECT '. implode(', ', $params) . '  FROM `tasks` '
                . 'ORDER BY ' . $orderNumber . ' ' . $modifier
                . ' LIMIT :limit '
                . 'OFFSET :offset '
        ;

        $sth = $this->pdo->prepare($sql);
        $sth->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $sth->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $sth->execute();

        $response= $sth->fetchAll(\PDO::FETCH_ASSOC);
        $result = [];
        foreach ($response as $item) {
            $result[] = $this->initTask($item);
        }

        return $result;
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function save(Task $task) : bool
    {
        if ($task->getId() > 0) {
            $sth = $this->pdo->prepare(
                'UPDATE  `tasks` '
                . 'SET  `name` = :name, `email` = :email, `text` = :text, `status`= :status, `changed`=:changed '
                . 'WHERE `id`=:id  LIMIT 1'
            );
            $sth->bindValue(':id', $task->getId(), \PDO::PARAM_INT);

            $old_task = $this->get($task->getId());
            $task->setChanged($old_task->isChanged());
            if ($old_task->getHash() !== $task->getHash()) {
                $task->setChanged(true);
            }

            $sth->bindValue(':changed', $task->isChanged(), \PDO::PARAM_INT);

        } else {
            $sth = $this->pdo->prepare(
                'INSERT INTO `tasks` (name, email, text, status) '
                . 'VALUES(:name, :email, :text, :status)'
            );
        }

        $sth->bindValue(':name', $task->getName(), \PDO::PARAM_STR);
        $sth->bindValue(':email', $task->getEmail(), \PDO::PARAM_STR);
        $sth->bindValue(':text', $task->getText(), \PDO::PARAM_STR);
        $sth->bindValue(':status', $task->isStatus() ?? false, \PDO::PARAM_BOOL);
        $sth->execute();

        return true;
    }

    /**
     * @param array $data
     * @return Task
     */
    private function initTask(array $data) : Task
    {
        $task = new Task($data['id']);
        $task->setName($data['name']);
        $task->setText($data['text']);
        $task->setEmail($data['email']);
        $task->setStatus($data['status']);
        $task->setChanged($data['changed']);
        return $task;
    }
}
