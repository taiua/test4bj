<?php


namespace App\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * Class TaskController
 * @package App\Controller
 */
class TaskController
{
    /** @var Environment */
    private $twig;

    /** @var Request */
    protected $request;

    /** @var string */
    protected $sort;

    /** @var string */
    protected $sortModifier ;



    /**
     * TaskController constructor.
     * @param Environment $twig
     * @param Request $request
     */
    public function __construct(Environment $twig, Request $request)
    {
        $this->twig = $twig;
        $this->request = $request;

        $sort = $this->request->get('sort') ?? $this->request->getSession()->get('sort');
        $sortModifier = $this->request->get('sort_modifier') ?? $this->request->getSession()->get('sort_modifier');

        if (in_array($sort, ['id', 'name', 'email', 'status'])) {
            $this->sort = $sort;
            $this->request->getSession()->set('sort', $sort);
        } else {
            $this->sort = 'id';
        }

        if (in_array($sortModifier, ['ASC', 'DESC'])) {
            $this->sortModifier = $sortModifier;
            $this->request->getSession()->set('sort_modifier', $sortModifier);
        } else {
            $this->sortModifier =  'DESC';
        }
    }

    /**
     * @param string $name
     * @param array $context
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render(string $name, array $context = []) : string
    {
        $context['user'] = UserService::isLogin($this->request);
        $context['sort'] = $this->sort;
        $context['sort_modifier'] = $this->sortModifier;
        return  $this->twig->render($name, $context);
    }

    /**
     * @param TaskRepository $taskRepository
     * @param int|null $page
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function list(TaskRepository $taskRepository, int $page = null) : string
    {
        if ($page === null || $page <= 1) {
            $page = 0;
        } else {
            $page--;
        }

        /** @var  Task $task */
        $tasks = $taskRepository->getList(3, $page * 3, $this->sort, $this->sortModifier);

        return $this->render(
            'list.html.twig',
            [
                'tasks' => $tasks,
                'pagination' => $this->pagination(($page+1), $taskRepository->count()),
            ]
        );
    }

    /**
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function form() : string
    {
        return $this->render('form.html.twig');
    }

    /**
     * @param TaskRepository $taskRepository
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function add(TaskRepository $taskRepository) : string
    {
        $task = new Task();

        return $this->render(
            'add.html.twig',
            $this->persistTask($task, $this->request, $taskRepository)
        );
    }

    /**
     * @param int $page
     * @param int $count
     * @return array
     */
    protected function pagination(int $page, int $count) : array
    {
        $count = ceil($count/3);

        $pagination = [
            'previous' => 0,
            'next' => 0,
            'pages' => []
        ];

        if ($page < $count) {
            $pagination['next'] = $page+1;
        }

        if ($page > 1) {
            $pagination['previous'] = $page-1;
            $pagination['pages'][] = $page-1;
        }


        for ($i = 0; $i < 3 && $page <= $count; $i++, $page++) {
            if ($page === 0) {
                continue;
            }
            $pagination['pages'][] = $page;
        }

        return $pagination;
    }

    /**
     * @param Task $task
     * @param Request $request
     * @param TaskRepository $taskRepository
     * @return array
     */
    protected function persistTask(Task $task, Request $request, TaskRepository $taskRepository) : array
    {
        $errors = [];

        $name = $request->get('name');
        $email = $request->get('email');
        $text = $request->get('text');
        $status = $request->get('status', null);

        if (strlen($name) === 0) {
            $errors[] = 'Неверно заполнено имя';
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'Неверно заполнено email';
        }

        if (strlen($text) === 0) {
            $errors[] = 'Неверно заполнено text';
        }

        $result = '';

        if (empty($errors)) {
            $task->setName($name);
            $task->setEmail($email);
            $task->setText($text);
            if (UserService::isLogin($request) && $status !== null) {
                $task->setStatus($status);
            }

            $taskRepository->save($task);
            if ($task->getId() > 0) {
                $result = 'Таск успешно обновлен!';
            } else {
                $result = 'Таск успешно добавлен!';
            }
        }

        return ['errors' => $errors, 'result' => $result];
    }
}
