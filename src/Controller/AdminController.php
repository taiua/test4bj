<?php

namespace App\Controller;

use App\Entity\Task;
use App\Repository\TaskRepository;
use App\Service\UserService;

/**
 * Class AdminController
 * @package App\Controller
 */
class AdminController extends TaskController
{
    /**
     * @param TaskRepository $taskRepository
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function login(TaskRepository $taskRepository) : string
    {
        if (UserService::isLogin($this->request) === false) {
            return $this->render('admin/login.html.twig');
        }

        return $this->auth($taskRepository);
    }

    /**
     * @param TaskRepository $taskRepository
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function auth(TaskRepository $taskRepository) : string
    {
        if (UserService::isLogin($this->request) === false) {
            $login = trim($this->request->get('login'));
            $password = trim($this->request->get('password'));

            if ($login !== 'admin' || $password !== '123') {
                return $this->render('admin/login.html.twig', ['alert' => 'Введен неверный логин или пароль']);
            }

            UserService::login($this->request);
        }

        return $this->list($taskRepository);
    }

    /**
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function logout() : string
    {
        UserService::logout($this->request);
        return $this->render('admin/login.html.twig');
    }

    /**
     * @param int $id
     * @param TaskRepository $taskRepository
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function updateTask(int $id, TaskRepository $taskRepository) : string
    {
        if (UserService::isLogin($this->request) === false) {
            return $this->render('admin/login.html.twig');
        }

        $task = new Task($id);
        $result = $this->persistTask($task, $this->request, $taskRepository);
        return $this->render('add.html.twig', $result);
    }

    /**
     * @param int $id
     * @param TaskRepository $taskRepository
     * @return string
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getTask(int $id, TaskRepository $taskRepository) : string
    {
        if (UserService::isLogin($this->request) === false) {
            return $this->render('admin/login.html.twig');
        }

        /** @var  Task $task */
        $task = $taskRepository->get($id);
        if (null === $task) {
            return $this->render('404.html.twig');
        }

        return $this->render('admin/update.html.twig', ['task' => $task]);
    }
}
