<?php


namespace App;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class Core
{
    /** @var ContainerBuilder */
    protected $containerBuilder;


    /** @var string */
    protected $rootDir;


    public function __construct()
    {
        $this->rootDir = __DIR__ . '/..';
        $this->initDI();
    }


    public function initDI()
    {
        $containerBuilder = new ContainerBuilder();
        $containerBuilder->setParameter('root_dir', $this->getRootDir());

        $request = Request::createFromGlobals();
        $request->setSession(new Session());
        $request->getSession()->start();
        $containerBuilder->set(Request::class, $request);

        $loader = new YamlFileLoader($containerBuilder, new \Symfony\Component\Config\FileLocator(__DIR__));
        $loader->load($this->rootDir . '/config/services.yaml');
        $containerBuilder->compile();
        $this->containerBuilder = $containerBuilder;
    }

    /**
     * @return string
     */
    public function getRootDir(): string
    {
        return $this->rootDir;
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainerBuilder(): ContainerBuilder
    {
        return $this->containerBuilder;
    }
}
