<?php


namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

class UserService
{
    public static function isLogin(Request $request) : bool
    {
        if ($request->hasSession() === false || $request->getSession()->isStarted() === false) {
            return false;
        }

        if ($request->getSession()->get('user', '') !== 'login') {
            return false;
        }

        return true;
    }

    public static function login(Request $request) : bool
    {
        if ($request->getSession()->isStarted() === false) {
            $request->getSession()->start();
        }

        $request->getSession()->set('user', 'login');
        $request->getSession()->save();
        return true;
    }

    public static function logout(Request $request) : bool
    {
        if ($request->getSession()->isStarted()) {
            $request->getSession()->set('user', '');
            $request->getSession()->save();
        }

        return true;
    }
}
