<?php


namespace App\Entity;


class Task
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $email;

    /**
     * @var bool
     */
    private $status;

    /**
     * @var bool
     */
    private $changed;

    public function __construct($id = null)
    {
        $this->id = $id;
        $this->status = false;
        $this->changed = false;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return bool|null
     */
    public function isStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(?bool $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return bool|null
     */
    public function isChanged(): ?bool
    {
        return $this->changed;
    }

    /**
     * @param bool $changed
     */
    public function setChanged(?bool $changed): void
    {
        $this->changed = $changed;
    }

    public function getHash()
    {
        return md5($this->email . $this->name. $this->text);
    }

}
